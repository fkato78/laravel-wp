<?php

namespace App\Http\Controllers;

use App\Category;
use App\Post;
use Illuminate\Http\Request;

class PostController extends Controller
{
    // Pagination limit
    protected $limit = 5;

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * All posts
     */
    public function index()
    {
        $categories = Category::with('posts')->orderBy('name', 'desc')->get();
        // Query debug
//        \DB::enableQueryLog();
        $posts = Post::with('author')
                        ->latestFirst()
                        ->published()
                        ->simplePaginate($this->limit);
         return view('blog.index', compact('posts', 'categories'));
//         view('blog.index', compact('posts'))->render();
//        dd(\DB::getQueryLog());
    }


    /**
     * All categories
     * @param Category $category
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */

    public function category( Category $category)
    {

//        $posts = Post::with('author')
//            ->latestFirst()
//            ->published()
//            ->where('category_id', $id)
//            ->simplePaginate($this->limit);
        $posts =$category->posts()
                         ->with('author')
                         ->published()
                         ->simplePaginate($this->limit);
        return view('blog.index', compact('posts'));
    }

    public function show(Post $post)
    {
        // increment post count_view
        $post->increment('view_count', 1);
        // Route Model Binding
        /**
         * refer to RouteServiceProvider file boot() method
         */
        return view('blog.show', compact('post'));
    }
}
