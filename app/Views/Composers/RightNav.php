<?php
/**
 * Created by PhpStorm.
 * User: fares
 * Date: 20.04.18
 * Time: 23:44
 */

namespace App\Views\Composers;

use App\Category;
use App\Post;
use Illuminate\View\View;

/**
 * Class RightNav
 * @package App\Views\Composers
 * service provider class to display categories and popular posts on the sidebar view
 */
class RightNav
{
    public function compose(View $view)
    {
        $this->composeCategories($view);
        $this->composePopularPosts($view);
    }

    public function composeCategories(View $view)
    {
        $categories = Category::with(['posts' => function ($query) {
            $query->published();
        }])->orderBy('name', 'desc')->take(5)->get();
        $view->with('categories', $categories);

    }

    public function composePopularPosts(View $view)
    {
        $popularPosts = Post::published()->popular()->take(3)->get();
        $view->with('popularPosts', $popularPosts);

    }

}