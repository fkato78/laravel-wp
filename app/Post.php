<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable = ['title', 'slug', 'body', 'excerpt', 'image', 'author_id'];

    /**
     * @var array
     * Add published_at to date object
     */
    protected $dates = ['published_at'];

    /**
     * @return string
     * Create accessor to get post image url
     */
    public function getImageUrlAttribute()
    {
        // default no found image
        $imageUrl = asset("img/no_image.png");

        if(!is_null($this->image)){
            $imagePath = public_path(). '/img/'. $this->image;
            if(file_exists($imagePath)){
                $imageUrl = asset('img/'. $this->image);
            }
        }
        return $imageUrl;
    }

    /**
     * @return string
     * Create accessor to get post thumbnail url
     */
    public function getImageThumbUrlAttribute()
    {
        // default no found image
        $thumbUrl = asset("img/no_thumb.png");

        if(!is_null($this->thumbnail)){
            $thumbePath = public_path(). '/img/'. $this->thumbnail;
            if(file_exists($thumbePath)){
                $thumbUrl = asset('img/'. $this->thumbnail);
            }
        }
        return $thumbUrl;
    }


    /**
     * relation ship with user(author)
     */
    public function author()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * create scope for created_at
     */
    public function scopeLatestFirst($query)
    {
        return $query->orderBy('created_at', 'desc');
    }

    /**
     * create scope for popular posts
     */
    public function scopePopular($query)
    {
        return $query->orderBy('view_count', 'desc');
    }

    /**
     *display the posts status
     */
    public function displayStatus()
    {
       if($this->published_at == null){
           return "<span class='label label-warning'>Draft</span>";
           // isFuture() is Carbon method
       }elseif($this->published_at && $this->published_at->isFuture()){
           return "<span class='label label-info'>Scheduled</span>";
       }else{
           return "<span class='label label-success'>Published</span>";
       }
    }


    /**
     * @return mixed
     * Create published_at accessor
     */
    public function getDateAttribute()
    {
        return (is_null($this->published_at)) ? '' : $this->published_at->diffForHumans();
    }

    /**
     * create scope for created_at
     */
    public function scopePublished($query)
    {
        return $query->where('published_at', "<=" , Carbon::now());
    }

    /**
     * Relation ship with category model
     */
    public function category()
    {
        return $this->belongsTo(Category::class);
    }

}
