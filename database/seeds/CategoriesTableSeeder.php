<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

use Faker\Factory as Faker;


class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        foreach (range(1, 10) as $index) {
            $image = 'Post_Image_' . rand(1, 10) . '.jpg';
            DB::table('categories')->insert([
                'name' => $faker->sentence($nbWords = 2, $variableNbWords = true),
                'slug' => $faker->slug,
                'body' => $faker->paragraph,
                'image' => rand(0, 1) == 1 ? $image : null,
                'created_at' => $faker->dateTime($max = 'now'),
                'updated_at' => $faker->dateTime($max = 'now')
            ]);
        }
    }
}
