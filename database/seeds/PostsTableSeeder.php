<?php

use Illuminate\Database\Seeder;

use Illuminate\Support\Facades\DB;

use Faker\Factory as Faker;

class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        foreach (range(1, 10) as $index) {
            $image = 'Post_Image_' . rand(1, 5) . '.jpg';
            $thumbnail = 'Post_Image_' . rand(1, 5) . '_thumb.jpg';
            DB::table('posts')->insert([
                'author_id' => rand(1, 3),
                'category_id' => rand(1, 3),
                'view_count' => rand(1, 50),
                'title' => $faker->catchPhrase,
                'slug' => $faker->slug,
                'body' => $faker->paragraph,
                'excerpt' => $faker->text(rand(250, 300)),
                'image' => rand(0, 1) == 1 ? $image : null,
                'thumbnail' => rand(0, 1) == 1 ? $thumbnail : null,
                'created_at' => $faker->dateTime($max = 'now'),
                'updated_at' => $faker->dateTime($max = 'now')
            ]);
        }
    }
}
