@extends('layouts.master')


@section('title', 'Page Not Found')

@section('content')
    <div class="container">
        <div class="row"><div class="col-lg-12"><h1 class="text-center alert-danger">Page not found</h1></div></div>
    </div>


@endsection