@extends('layouts.master')
@section('title') {{ env('APP_NAME') }} @endsection
@section("content")
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                @if($posts->count() != 0)
                    @foreach($posts as $post)
                        <article class="post-ite">
                            <div class="post-item-image">
                                <a href="{{ route('blog.show', $post->slug) }}">
                                    <img src="{{ $post->image_url }}" alt="{{ $post->slug }}">
                                </a>
                            </div>
                            <div class="post-item-body">
                                <div class="padding-10">
                                    <h2><a href="{{ route('blog.show', $post->slug) }}">{{ $post->title }}</a></h2>
                                    <p>{!! $post->body !!}</p>
                                </div>

                                <div class="post-meta padding-10 clearfix">
                                    <div class="pull-left">
                                        <ul class="post-meta-group">
                                            <li><i class="fa fa-user"></i><strong> {{ $post->author->name }}</strong></li>
                                            <li><i class="fa fa-clock-o"></i><time> {{ $post->date }}</time></li>
                                            <li><i class="fa fa-folder"></i><a href="{{ route('category', $post->category->slug) }}">{{ $post->category->name }}</a></li>
                                            <li><i class="fa fa-comments"></i><a href="#">4 Comments</a></li>
                                        </ul>
                                    </div>
                                    <div class="pull-right">
                                        <a href="post.html">Continue Reading &raquo;</a>
                                    </div>
                                </div>
                            </div>
                        </article>
                    @endforeach
                    {{ $posts->links() }}
                @else
                    <div class="alert alert-warning">
                        Nothing found
                    </div>
                @endif
            </div>
@include("partials._sidebar")
        </div>
    </div>
@endsection





