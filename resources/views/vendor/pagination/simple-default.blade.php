@if ($paginator->hasPages())
    <nav>
        <ul class="pager">
            {{-- Previous Page Link --}}
            @if ($paginator->onFirstPage())
                <li class=" previous disabled"><span>@lang('pagination.previous')</span></li>
            @else
                <li class="previous"><a href="{{ $paginator->previousPageUrl() }}" rel="prev"><span aria-hidden="true"></span>@lang('pagination.previous')</a></li>
            @endif
            @if ($paginator->hasMorePages())
                <li class="next"><a href="{{ $paginator->nextPageUrl() }}" rel="next"><span aria-hidden="true"></span>@lang('pagination.next')</a></li>
            @else
                <li class="next disabled"><span>@lang('pagination.next')</span></li>
            @endif
        </ul>
    </nav>
@endif
