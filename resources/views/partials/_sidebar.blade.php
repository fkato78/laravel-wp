<div class="col-md-4">
    <aside class="right-sidebar">
        <!-- Search
        <div class="search-widget">
            <div class="input-group">
                <input type="text" class="form-control input-lg" placeholder="Search for...">
                <span class="input-group-btn">
                            <button class="btn btn-lg btn-default" type="button">
                                <i class="fa fa-search"></i>
                            </button>
                          </span>
            </div>
        </div>
        -->
        <div class="widget">
            <div class="widget-heading">
                <h4>Categories</h4>
            </div>
            <div class="widget-body">
                <ul class="categories">
                    @foreach($categories as $category)
                    <li>
                        <a href={{ route('category', $category->slug) }}><i class="fa fa-angle-right"></i>&nbsp;{{ $category->name }}</a>
                        <span class="badge pull-right">{{ $category->posts->count() }}</span>
                    </li>
                    @endforeach
                </ul>
            </div>
        </div>

        <div class="widget">
            <div class="widget-heading">
                <h4>Popular Posts</h4>
            </div>
            <div class="widget-body">
                <ul class="popular-posts">
                    @foreach($popularPosts as $popPost)
                    <li>
                        <div class="post-image">
                            <a href="{{ route('blog.show', $popPost->slug) }}">
                                <img src="{{ $popPost->image_thumb_url }}" />
                            </a>
                        </div>
                        <div class="post-body">
                            <h6><a href="{{ route('blog.show', $popPost->slug) }}">{{ $popPost->title }}</a></h6>
                            <div class="post-meta">
                                <span>{{ $popPost->date }}</span>
                            </div>
                        </div>
                    </li>
                    @endforeach
                </ul>
            </div>
        </div>
        <!-- Tags
        <div class="widget">
            <div class="widget-heading">
                <h4>Tags</h4>
            </div>
            <div class="widget-body">
                <ul class="tags">
                    <li><a href="#">PHP</a></li>
                    <li><a href="#">Codeigniter</a></li>
                    <li><a href="#">Yii</a></li>
                </ul>
            </div>
        </div>
        -->
    </aside>
</div>