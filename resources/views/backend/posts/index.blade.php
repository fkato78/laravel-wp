@extends('layouts.dashboard')
@section('title') Blog | index @endsection

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                <strong>All posts</strong>
                <a href="" class="btn btn-success btn-lg pull-right">Add new post</a>
            </h1>
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-body ">
                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <td>Title</td>
                                    <td>Author</td>
                                    <td>Category</td>
                                    <td>Date</td>
                                    <td>Status</td>
                                    <td>Actions</td>
                                </tr>
                                </thead>
                                <tbody>
                                @if($postsCount > 0)
                                @foreach($posts as $post)
                                    <tr>
                                        <td>{{ $post->title }}</td>
                                        <td>{{ $post->author->name }}</td>
                                        <td>{{ $post->category->name }}</td>
                                        <td>
                                            {{ $post->created_at->diffForHumans() }}
                                        </td>
                                        <td> {!! $post->displayStatus() !!}</td>
                                        {{--<td>{{ $post->future() }}</td>--}}
                                        <td width="100">
                                            <a href="" class="btn btn-success btn.xs"><i class="fa fa-edit"></i></a>
                                            <a href="" class="btn btn-danger btn.xs"><i class="fa fa-remove"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                                @else
                                    <div class="alert-warning">
                                        <h1 class="text-center">No posts found</h1>
                                    </div>
                                @endif
                                </tbody>
                            </table>
                        </div>
                        <div class="box-footer">
                            <div class="pull-left">
                                {{ $posts->links() }}
                            </div>
                            <div class="pull-right"><small>{{ $postsCount }} item</small></div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
            </div>
            <!-- ./row -->
        </section>
        <!-- /.content -->
    </div>
@endsection
